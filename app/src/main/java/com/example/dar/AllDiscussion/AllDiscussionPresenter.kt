package com.example.dar.AllDiscussion

import android.content.Context
import android.util.Log
import com.example.dar.Discussion.DiscussionSession
import com.example.dar.FirebaseDataBaseReference
import com.example.dar.PerformanceInterface
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener


class AllDiscussionPresenter (val context: Context, val performanceInterface: PerformanceInterface) {
    private val databaseReference = FirebaseDataBaseReference.firebasereference()
    var allSession = mutableListOf<DiscussionSession>()
    var allSessionKey = mutableListOf<String>()

    fun getAllSession() {
        //Log.d("viewModel","viewModel")
        databaseReference.child("discussionSession").addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                p0.toException()
            }
            override fun onDataChange(p0: DataSnapshot) {
                val child = p0.children
                child.forEach {
                    allSessionKey.add(it.key.toString())
                    val session = it.getValue(DiscussionSession::class.java)
                    if (session != null) {
                        allSession.add(session)
                    }
                }
                performanceInterface.getData(allSession,allSessionKey)
            }
        })
    }


}