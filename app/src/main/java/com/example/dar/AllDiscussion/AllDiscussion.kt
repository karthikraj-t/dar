package com.example.dar.AllDiscussion


import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.ProgressBar
import androidx.appcompat.app.ActionBar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.dar.Discussion.Discussion
import com.example.dar.Discussion.DiscussionSession
import com.example.dar.DiscussionCommend.DisplaySession
import com.example.dar.PerformanceInterface
import com.example.dar.R
import com.facebook.shimmer.ShimmerFrameLayout
import com.google.android.material.floatingactionbutton.FloatingActionButton

class AllDiscussion : AppCompatActivity(),PerformanceInterface {

    lateinit var list: RecyclerView
    lateinit var progressBar: ProgressBar
    lateinit var shimmer_view_container:ShimmerFrameLayout
    var toolbar: ActionBar? = null
    lateinit var allSession:MutableList<DiscussionSession>
    lateinit var allDiscussionPresenter: AllDiscussionPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_all_discussion)

        toolbar = supportActionBar
        toolbar?.setTitle("All Discussion")
        toolbar?.setDisplayHomeAsUpEnabled(true)

        list = findViewById(R.id.list)
        shimmer_view_container =findViewById(R.id.shimmer_view_container)
        shimmer_view_container.startShimmer()

        val addDiscusstion = findViewById<FloatingActionButton>(R.id.addDiscussion)
        addDiscusstion.setOnClickListener {
            val intent = Intent(this, Discussion::class.java)
            startActivity(intent)
        }

        allDiscussionPresenter = AllDiscussionPresenter(this, this)
        allDiscussionPresenter.getAllSession()
        Log.d("viewModel","discussion")

    }

    @SuppressLint("WrongConstant")
    override fun getData(allSession: MutableList<DiscussionSession>, allSessionKey: MutableList<String>) {
        list.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL,false)
        val adapter = DiscussionListItems(allSession,allSessionKey,this)
        list.adapter = adapter

        shimmer_view_container.stopShimmer()
        shimmer_view_container.visibility = View.GONE
    }

    override fun viewSession(sessionId: String, session: DiscussionSession) {
        val intent = Intent(this, DisplaySession::class.java)
        intent.putExtra("sessionId",sessionId)
        intent.putExtra("title",session.title)
        intent.putExtra("desc",session.description)
        intent.putExtra("date",session.date)
        intent.putExtra("time",session.time)
        intent.putExtra("email",session.email)
        intent.putExtra("name",session.name)
        startActivity(intent)
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        finish()
        return true
    }
}
