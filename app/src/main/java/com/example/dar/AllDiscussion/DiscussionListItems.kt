package com.example.dar.AllDiscussion


import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.dar.Discussion.DiscussionSession
import com.example.dar.PerformanceInterface
import com.example.dar.R



class DiscussionListItems(
    var allMySession: MutableList<DiscussionSession>,
    var allSessionKey: MutableList<String>,
    var performanceInterface: PerformanceInterface
) : RecyclerView.Adapter<DiscussionListItems.ViewHolder>(){

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){

        fun bindView(session: DiscussionSession,key:String){
            val title: TextView =itemView.findViewById(R.id.title)
            val description: TextView = itemView.findViewById(R.id.description)
            val date: TextView = itemView.findViewById(R.id.date)

            itemView.setTag(key)
            title.setText(session.title)
            description.setText(session.description)
            date.setText(session.name+" - "+session.date)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.activity_discussion_list_items,parent,false)
        return ViewHolder(v)
    }

    override fun getItemCount() = allMySession.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val session =allMySession[position]
        val sessionId =allSessionKey.get(position)
        holder.bindView(session,sessionId)
        holder.itemView.setOnClickListener{
            performanceInterface.viewSession(sessionId,session)
        }
    }
}
