package com.example.dar.Signin


import android.content.Context
import androidx.fragment.app.FragmentActivity
import com.example.dar.FirebaseDataBaseReference
import com.example.dar.R
import com.example.dar.SigninInterface
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider

class SigninPresenter(val context: Context,
                      val signinInterface: SigninInterface,
                      val fragmentActivity: FragmentActivity
): GoogleApiClient.OnConnectionFailedListener {
    lateinit var googleApiClient: GoogleApiClient

    fun googleSignin(){

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(context.getString(R.string.web_client_id))
            .requestEmail()
            .build()

        googleApiClient = GoogleApiClient.Builder(context)
            .enableAutoManage(fragmentActivity,this)
            .addApi(Auth.GOOGLE_SIGN_IN_API,gso)
            .build()
        signinInterface.signIn(googleApiClient)
    }

    fun firebaseAuthWithGoogle(firebaseAuth:FirebaseAuth,acct: GoogleSignInAccount){
        val credential = GoogleAuthProvider.getCredential(acct.idToken,null)

        val firebaseDatabaseReference= FirebaseDataBaseReference.firebasereference()

        firebaseAuth.signInWithCredential(credential)
            .addOnCompleteListener(fragmentActivity) { task ->
                if(task.isSuccessful){
                    firebaseDatabaseReference.child("Emp").child(acct.id.toString()).child("name").setValue(acct.displayName)
                    signinInterface.signInfirebase(true)
                }
                else{
                    signinInterface.signInfirebase(false)
                }
            }
    }

    override fun onConnectionFailed(p0: ConnectionResult) {

    }
}