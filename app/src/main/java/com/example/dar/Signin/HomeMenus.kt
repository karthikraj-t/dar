package com.example.dar.Signin

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.example.dar.R


class HomeMenus(val context: Context,val logos:Array<Pair<String,Int>>) :BaseAdapter() {

    override fun getView(p0: Int, p1: View?, viewGroup: ViewGroup?): View {
        var inflater = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        var disview = inflater.inflate(R.layout.activity_home_menus,null)
        var imageView:ImageView = disview.findViewById(R.id.image)
        var menuname:TextView = disview.findViewById(R.id.menuName)

        menuname.setText(logos[p0].first)
        imageView.setImageResource(logos[p0].second)
        return disview
    }

    override fun getItem(p0: Int): Any {
        return ""
    }

    override fun getItemId(p0: Int): Long {
        return 0
    }
    override fun getCount() = logos.size
}