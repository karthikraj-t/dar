package com.example.dar.Signin

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.widget.*
import com.example.dar.AllDiscussion.AllDiscussion
import com.example.dar.AllTimeLine.TimeLineBottomNavBar
import com.example.dar.R
import com.example.dar.WorkFrom.WorkFrom
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.firebase.auth.FirebaseAuth


class Home : AppCompatActivity() {

    lateinit var googleApiClient:GoogleApiClient
    lateinit var  displayName:TextView
    lateinit var sharedPreferences: SharedPreferences
    lateinit var email:TextView
    lateinit var gridView: GridView
    val logos = arrayOf(Pair("TimeLine",R.drawable.ic_timeline),
                    Pair("Work",R.drawable.ic_work),
                    Pair("All Discussion",R.drawable.ic_alldiscussion))

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        sharedPreferences = getSharedPreferences("user_details", Context.MODE_PRIVATE)

        displayName = findViewById(R.id.displayName)
        email = findViewById(R.id.emailID)
        gridView = findViewById(R.id.gridview)

        val homeMenus = HomeMenus(this, logos)
        gridView.adapter = homeMenus


        gridView.setOnItemClickListener { adapterView, view, i, l ->
            intent(logos[i].first)
        }

        val intent:Intent = getIntent()
        updateProfile(intent)

        val signout:ImageView = findViewById(R.id.signout)

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.web_client_id))
            .requestEmail()
            .build()

        googleApiClient = GoogleApiClient.Builder(this)
            .enableAutoManage(this,GoogleApiClient.OnConnectionFailedListener {  })
            .addApi(Auth.GOOGLE_SIGN_IN_API,gso)
            .build()
        signout.setOnClickListener {
            MaterialAlertDialogBuilder(this)
                .setMessage("Do you want to logout?")
                .setPositiveButton("yes",{ dialogInterface, i ->
                        FirebaseAuth.getInstance().signOut()
                        Auth.GoogleSignInApi.signOut(googleApiClient)
                        val intent = Intent(this, SignIn::class.java)
                        startActivity(intent)
                })
                .setNegativeButton("no",{ dialogInterface, i ->
                        dialogInterface.cancel()
                })
                .show()

        }
    }

    fun updateProfile(intent:Intent){
        displayName.text = sharedPreferences.getString("displayName",null)
        email.text = sharedPreferences.getString("email",null)
    }

    override fun onBackPressed() {
        val dialog = android.app.AlertDialog.Builder(this)
        dialog.setMessage("Do you want to exit?")
            .setPositiveButton("Yes",DialogInterface.OnClickListener{
                    dialogInterface, i ->
                finish()
                System.exit(0)
            }).setNegativeButton("No",DialogInterface.OnClickListener{
                    dialogInterface, i ->
                dialogInterface.cancel()
            })
        dialog.show()
    }
    fun intent(activity:String){
        val i:Intent?
        i =  when(activity){
            "TimeLine" ->  Intent(this,TimeLineBottomNavBar::class.java)
            "Work" -> Intent(this,WorkFrom::class.java)
            "All Discussion" ->  Intent(this,AllDiscussion::class.java)
            else -> null
        }
        startActivity(i)
    }
}
