package com.example.dar.Signin

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.core.widget.ContentLoadingProgressBar
import com.example.dar.R
import com.example.dar.SigninInterface
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.*
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.GoogleApiClient
import com.google.firebase.auth.FirebaseAuth


class SignIn : AppCompatActivity(),GoogleApiClient.OnConnectionFailedListener,SigninInterface{

    private val RC_SIGN_IN = 1

    lateinit var firebaseAuth: FirebaseAuth

    lateinit var sharedPreferences: SharedPreferences

    lateinit var signinPresenter: SigninPresenter

    var account: GoogleSignInAccount? =null
    lateinit var progressBar: ContentLoadingProgressBar
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)

        progressBar = findViewById(R.id.progress_Bar)
        progressBar.visibility = View.GONE
        firebaseAuth = FirebaseAuth.getInstance()

        sharedPreferences = getSharedPreferences("user_details", Context.MODE_PRIVATE)

        signinPresenter = SigninPresenter(this,this,this)

        val signinwithgoogle: ImageView = findViewById(R.id.signinwithgoogle)

        signinwithgoogle.setOnClickListener {
            signinPresenter.googleSignin()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            val task = Auth.GoogleSignInApi.getSignInResultFromIntent(data) //GoogleSignIn.getSignedInAccountFromIntent(data)
            try{
                account= task.signInAccount //task.getResult(ApiException::class.java)
                account?.let { signinPresenter.firebaseAuthWithGoogle(firebaseAuth, it) }
            }catch (e:ApiException){
                e.printStackTrace()
            }
        }
    }

    override fun signIn(googleApiClient: GoogleApiClient){
        val signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient)  //googleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
        progressBar.visibility = View.VISIBLE
    }
    override fun signInfirebase(rs: Boolean) {
        if(rs){
            Toast.makeText(this,"Sign in success ${account?.displayName}", Toast.LENGTH_LONG).show()
            val intent = Intent(this, Home::class.java)


            val editer: SharedPreferences.Editor = sharedPreferences.edit()
            editer.putString("Uid",account?.id.toString())
            editer.putString("displayName",account?.displayName)
            editer.putString("email",account?.email)
            editer.apply()
            editer.commit()
            progressBar.visibility = View.GONE
            startActivity(intent)
            finish()
        }else{
            Toast.makeText(this,"Sign in failed", Toast.LENGTH_LONG).show()
            progressBar.visibility = View.GONE
        }
    }

    override fun onConnectionFailed(p0: ConnectionResult) {

    }

    override fun onStart() {
        super.onStart()
        val user = FirebaseAuth.getInstance().currentUser
        if(user != null){
            val intent = Intent(this, Home::class.java)
            startActivity(intent)
            finish()
        }
    }
}




