package com.example.dar.Timeline

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.dar.FirebaseDataBaseReference
import com.example.dar.WorkFrom.TimelineModel
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import java.text.SimpleDateFormat
import java.util.*

class TimeLineViewModel :ViewModel() {

    val firebaseDatabaseReference = FirebaseDataBaseReference.firebasereference()

    var todayTimeLine = mutableListOf<Pair<String?,Long?>>()

    private val users: MutableLiveData<List<Pair<String?,Long?>>> by lazy {
        MutableLiveData<List<Pair<String?,Long?>>>().also {
            loadUsers()
        }
    }

    private fun loadUsers() {

        val todayDatetime = getDateandTime()
       // Log.d("viewModel","firebase")
        firebaseDatabaseReference.child("Emp").addValueEventListener(object :ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {
                p0.toException()
            }
            override fun onDataChange(p0: DataSnapshot) {
                todayTimeLine.clear()
                val allemp:MutableIterable<DataSnapshot> = p0.children
                for(it in allemp)
                {
                    val name:String = it.child("name").value.toString()
                    val timeChile:MutableIterable<DataSnapshot>  = it.child("time").children
                    for(i in timeChile){
                        if(i.key.equals(todayDatetime[0])){
                            val intime = i.getValue(TimelineModel::class.java)
                            todayTimeLine.add(Pair(name,intime?.inTime))
                        }
                    }
                }
                users.value =  todayTimeLine.sortedBy { it -> it.second }
            }
        })
    }
    fun getUserData():LiveData<List<Pair<String?,Long?>>>{

        return users
    }

    fun getDateandTime():List<String>{
        val simpleDateFormat = SimpleDateFormat("dd-MM-yyyy hh:mm:ss aa E")
        val date = Date(System.currentTimeMillis())
        val today = simpleDateFormat.format(date)
        return today.split(" ")
    }
}