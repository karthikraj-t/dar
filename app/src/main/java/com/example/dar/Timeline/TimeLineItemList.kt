package com.example.dar.Timeline



import androidx.core.content.ContextCompat
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.dar.R
import java.text.SimpleDateFormat

class TimeLineItemList(val alltime:List<Pair<String?,Long?>>?): RecyclerView.Adapter<TimeLineItemList.ViewHolder>() {
    var rank =1
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v  = LayoutInflater.from(parent.context).inflate(R.layout.activity_time_line_item_list,parent,false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int = alltime!!.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val pair = alltime?.get(position)
        if (pair != null) {
            holder?.bindView(pair.first!!, pair.second!!,rank++)
        }
    }

    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {
        val sdf = SimpleDateFormat("HH:mm aa")
        fun bindView(name: String, inTime: Long, rank: Int){
            val rankCard = itemView.findViewById<CardView>(R.id.rankCard)
            val nameCard = itemView.findViewById<CardView>(R.id.nameCard)
            val nameT = itemView.findViewById<TextView>(R.id.name)
            val workfrom = itemView.findViewById<TextView>(R.id.workfrom)
            val rankT = itemView.findViewById<TextView>(R.id.rank)
            if(rank == 1){
                rankCard.setCardBackgroundColor(ContextCompat.getColor(itemView.context,R.color.darkblue))
                nameCard.setCardBackgroundColor(ContextCompat.getColor(itemView.context,R.color.darkblue))
                nameT.setTextColor(ContextCompat.getColor(itemView.context,R.color.white))
                workfrom.setTextColor(ContextCompat.getColor(itemView.context,R.color.white))
                rankT.setTextColor(ContextCompat.getColor(itemView.context,R.color.white))
            }

          //  Log.d("viewBindTag",name+"/"+inTime+"/"+rank)

            val date = sdf.format(inTime)
            val hours=date.split(":")[0].toInt()
            when(hours){
                8,9 ->{
                    rankT.text = "10"
                }
                10 ->{
                    rankT.text = "9"
                }
                11 ->{
                    rankT.text = "5"
                }
                else ->{
                    rankT.text = "1"
                }
            }
            nameT.text = name
            workfrom.text = sdf.format(inTime)
        }
    }

}