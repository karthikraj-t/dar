package com.example.dar.Timeline

import androidx.lifecycle.*
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.example.dar.R
import com.facebook.shimmer.ShimmerFrameLayout

class TimeLine : Fragment(){


    lateinit var recyclerView: RecyclerView

    lateinit var shimmerFrameLayout: ShimmerFrameLayout
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.activity_time_line, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        shimmerFrameLayout = view.findViewById(R.id.shimmer_view_container)
        shimmerFrameLayout.startShimmer()

        recyclerView = view.findViewById(R.id.list_item)

        val viewModel = ViewModelProviders.of(this)[TimeLineViewModel::class.java]
        viewModel.getUserData().observe(this, Observer<List<Pair<String?,Long?>>> { users ->
            recyclerView.layoutManager =
                LinearLayoutManager(view.context, LinearLayout.VERTICAL, false)
            recyclerView.adapter = TimeLineItemList(users)
            shimmerFrameLayout.stopShimmer()
            shimmerFrameLayout.visibility = View.GONE
           // Log.d("viewModel","viewModelTIme")
        })
    }
}
