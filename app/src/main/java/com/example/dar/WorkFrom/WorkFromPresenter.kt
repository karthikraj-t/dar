package com.example.dar.WorkFrom

import android.util.Log
import android.view.View
import android.widget.Toast
import com.example.dar.FirebaseDataBaseReference
import com.example.dar.WorkFromInterface
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import java.text.SimpleDateFormat
import java.util.*

class WorkFromPresenter(val workFromInterface: WorkFromInterface) {

    private val databaseReference = FirebaseDataBaseReference.firebasereference()

    fun saveLocation(uid:String,date:String,timelineModel: TimelineModel){
        databaseReference.child("Emp").child(uid).child("time").child(date).setValue(timelineModel)
            .addOnSuccessListener {
                workFromInterface.locationSaved("Saved")
            }
            .addOnFailureListener{
                workFromInterface.locationSaved("Failed")
                it.printStackTrace()
            }
    }

    fun getUserlocation(){
        databaseReference.child("office").addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                Log.d("office exception",p0.message)
                p0.toException()
            }
            override fun onDataChange(p0: DataSnapshot) {
                val a = p0.getValue(Office::class.java)
                if(a !=null){
                    workFromInterface.getOfficeLocation(a.lat!!, a.long!!)
                }
            }
        })
    }

    fun isUserloged(uid:String){
        //lateinit var todayDateTime:List<String>
        val todayDateTime = getDateandTime().split(" ")

        databaseReference.child("Emp").child(uid).child("time").addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                Log.d("office exception", p0.message)
                p0.toException()
            }
            override fun onDataChange(p0: DataSnapshot) {

                val chil = p0.children
                chil.forEach {
                    Log.d("key", it.key)
                    if(it.key == todayDateTime[0]){
                        val time = it.getValue(TimelineModel::class.java)
                        if (time != null) {
                            workFromInterface.isUserLoged(it.key!!,time)
                        }
                    }
                }
                workFromInterface.userNotLoged()
            }
        })
    }

    fun getDateandTime():String{
        val simpleDateFormat =SimpleDateFormat("dd-MM-yyyy HH:mm:ss aa E")
        val date = Date(System.currentTimeMillis())
        val today = simpleDateFormat.format(date.time)
        return today
    }

    fun distance(lat1: Double, lng1: Double, lat2: Double, lng2: Double): Double {      //find distance between two location

        val earthRadius = 3958.75 // in miles

        val dLat = Math.toRadians(lat2 - lat1)
        val dLng = Math.toRadians(lng2 - lng1)

        val sindLat = Math.sin(dLat / 2)
        val sindLng = Math.sin(dLng / 2)

        val a = Math.pow(sindLat, 2.0) + (Math.pow(sindLng, 2.0)
                * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)))

        val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))

        return earthRadius * c // output distance, in MILES
    }

}