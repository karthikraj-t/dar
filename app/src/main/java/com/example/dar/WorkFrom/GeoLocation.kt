package com.example.dar.WorkFrom

import android.annotation.SuppressLint
import android.content.Context
import android.os.Looper
import android.util.Log
import com.example.dar.WorkFromInterface
import com.google.android.gms.location.*

class GeoLocation(val context: Context,val workFromInterface: WorkFromInterface)  {

    var locationRequest = LocationRequest()
    val fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context)
    val locationCallback = object :LocationCallback(){
        override fun onLocationResult(p0: LocationResult?) {
            val locationList = p0?.locations
            locationList.let {
                val location = it?.last()
                if (location != null) {
                    if (!location.isFromMockProvider) {
                        workFromInterface.checkLocation(location.latitude,location.longitude)
                    }
                }
            }
        }
    }
    @SuppressLint("MissingPermission")
    fun getlocation(){
        locationRequest.interval = 10 * 1000
        locationRequest.fastestInterval = 2000
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        fusedLocationProviderClient.requestLocationUpdates(locationRequest,locationCallback, Looper.myLooper())
    }
    fun stopLocation(){
        fusedLocationProviderClient.removeLocationUpdates(locationCallback)
    }
}
