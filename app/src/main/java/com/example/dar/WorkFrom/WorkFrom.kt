package com.example.dar.WorkFrom

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.ActionBar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.dar.FirebaseDataBaseReference
import com.example.dar.R
import com.example.dar.Signin.Home
import com.example.dar.WorkFromInterface
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_work_from.*
import java.sql.Timestamp
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*


class WorkFrom : AppCompatActivity(),WorkFromInterface {

    lateinit var currentdate: TextInputEditText
    lateinit var inTime:TextInputEditText
    lateinit var save:Button
    lateinit var sharedPreferences: SharedPreferences
    lateinit var progressBar: ProgressBar
    val decimalFormat = DecimalFormat("##.######")

    private lateinit var databaseReference:DatabaseReference

    var officelat:Double =0.0
    var officelong: Double =0.0
    lateinit var myLocation: GeoLocation
    lateinit var workFrom:Spinner
    lateinit var todayDateTime:List<String>
    lateinit var workFromPresenter: WorkFromPresenter
    var Uid =""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_work_from)

        var toolbar: ActionBar? = null
        toolbar = supportActionBar
        toolbar?.setTitle("Work")
        toolbar?.setDisplayHomeAsUpEnabled(true)

        progressBar = findViewById(R.id.progress_Bar)
        progressBar.visibility = View.VISIBLE

        myLocation = GeoLocation(this,this)  // Location

        databaseReference = FirebaseDataBaseReference.firebasereference()

        sharedPreferences = getSharedPreferences("user_details", Context.MODE_PRIVATE)

        currentdate = findViewById(R.id.currentDate)
        inTime = findViewById(R.id.inTime)
        save = findViewById(R.id.save)

        workFrom = findViewById(R.id.workfrom)

        val workList = arrayOf("Office","Home")
        val arrayAdapter = ArrayAdapter(this,android.R.layout.simple_spinner_item,workList)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        workFrom.adapter = arrayAdapter
        workFrom.onItemSelectedListener = itemLisener

        todayDateTime = getDateandTime().split(" ")
        currentdate.setText(todayDateTime[0])
        inTime.setText(todayDateTime[1]+" "+todayDateTime[2])

        val perms = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION)

        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
            ||ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
        ) {

            ActivityCompat.requestPermissions(this,perms,101  )
        }

        Uid = sharedPreferences.getString("Uid","")

        workFromPresenter = WorkFromPresenter(this)
        workFromPresenter.getUserlocation()

        save.setOnClickListener {
            saveTime()
        }
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        finish()
        return true
    }

    private val itemLisener = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(p0: AdapterView<*>?) {

        }

        override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
            val item = p0?.selectedItem
                if(item!!.equals("Office")){
                    save.text = "Check Location"
                }
                else{
                    save.text = "Save"
                }
                Toast.makeText(this@WorkFrom,item.toString(),Toast.LENGTH_LONG).show()
        }
    }


    fun getDateandTime():String{
        val simpleDateFormat =SimpleDateFormat("dd-MM-yyyy HH:mm:ss aa E")
        val date = Date(System.currentTimeMillis())
        val today = simpleDateFormat.format(date.time)
        return today
    }

    fun saveTime(){

        val work_from = workfrom.selectedItem
        val date = currentdate.text
        val intime = inTime.text

        if (save.text.equals("Check Location")) {
            progressBar.visibility = View.VISIBLE
            myLocation.getlocation()
        } else {
            progressBar.visibility = View.VISIBLE
            val timestamp = Timestamp(System.currentTimeMillis())
            val time = TimelineModel(work_from.toString(), timestamp.time,todayDateTime[3])
            workFromPresenter.saveLocation(Uid,date.toString(),time)

        }
    }
    override fun locationSaved(msg: String) {
        toast(msg)
        progressBar.visibility = View.GONE
        val intent = Intent(this, Home::class.java)
        startActivity(intent)
        finish()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode)
        {
            101 ->{
                if(grantResults.isNotEmpty() && grantResults[0]== PackageManager.PERMISSION_GRANTED && grantResults[1]== PackageManager.PERMISSION_GRANTED)
                {
                    myLocation.getlocation()
                }
                else{
                    myLocation.getlocation()
                }
                return
            }
            else -> Log.d("permission","null")
        }
    }
    override fun checkLocation(cLat: Double, cLong: Double) {

        val mlat = decimalFormat.format(cLat).toDouble()
        val mlong = decimalFormat.format(cLong).toDouble()
        val distance = workFromPresenter.distance(mlat,mlong,officelat, officelong)

        if(distance != 0.0 && distance < 0.1){
            myLocation.stopLocation()
            toast("your in office")
            save.text = "Save"
        }else{
            toast("your not in office, Try again")
        }
        progressBar.visibility = View.GONE

        Log.d("mlat",mlat.toString())
        Log.d("mlong",mlong.toString())
        Log.d("distance",distance.toString())
    }
    override fun getOfficeLocation(lat: Double, long: Double) {
        officelat = lat
        officelong = long
        workFromPresenter.isUserloged(Uid)
    }

    override fun isUserLoged(date: String, timelineModel: TimelineModel) {
        save.visibility = View.GONE         // save button GONE
        workFrom.setSelection(
            if(timelineModel.from.equals("Office")){
                0
            }
            else{
                1
            }
        )
        currentdate.setText(date)
        val times =timelineModel.inTime
        val simpleDateFormat = SimpleDateFormat("HH:mm aa")
        inTime.setText(simpleDateFormat.format(times))
        progressBar.visibility = View.GONE
        toast("You're already Logged")
    }
    override fun userNotLoged() {
        progressBar.visibility = View.GONE
    }
    fun toast(msg:String){
        Toast.makeText(this,msg,Toast.LENGTH_LONG).show()
    }
}

