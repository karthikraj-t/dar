package com.example.dar.WorkFrom

data class Office(
    var lat:Double? =0.0,
    var long:Double? = 0.0)

data class TimelineModel(
    var from:String? ="",
    var inTime:Long? =0,
    var day:String? =""
)