package com.example.dar

import com.example.dar.WorkFrom.TimelineModel

interface WorkFromInterface {
    fun getOfficeLocation(lat:Double,long:Double)

    fun isUserLoged(date:String,timelineModel: TimelineModel)

    fun userNotLoged()

    fun checkLocation(cLat:Double,cLong:Double)

    fun locationSaved(msg:String)
}