package com.example.dar


import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

object FirebaseDataBaseReference {

    private val databaseReference =FirebaseDatabase.getInstance().reference

    fun firebasereference():DatabaseReference{
        return databaseReference
    }
}