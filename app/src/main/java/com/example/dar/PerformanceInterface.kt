package com.example.dar

import com.example.dar.Discussion.DiscussionSession

interface PerformanceInterface{

    fun getData(allSession:MutableList<DiscussionSession>,allSessionKey: MutableList<String>)

    fun viewSession(sessionId:String,session:DiscussionSession)

}