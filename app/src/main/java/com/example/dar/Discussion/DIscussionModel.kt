package com.example.dar.Discussion


data class DiscussionSession(
    var title:String? = "",
    var description:String? = "",
    var date:String? = "",
    var time:String? = "",
    var uid:String? = "",
    var email:String? = "",
    var name:String? = ""
)
data class QA(
    var sessionId:String? ="",
    var qa:String? ="",
    var email:String? = "",
    var name:String? = "",
    var time:String? = ""
)
