package com.example.dar.Discussion

import com.example.dar.DiscussionInterface
import com.example.dar.FirebaseDataBaseReference

class DiscussionPresenter(val discussionInterface: DiscussionInterface){

    val databaseReference = FirebaseDataBaseReference.firebasereference()

    fun postDiscussion(discussionSession: DiscussionSession){
        if(discussionSession.title?.trim().equals(""))
        {
            discussionInterface.inputValidate("title")
        }
        else if(discussionSession.description?.trim().equals(""))
        {
            discussionInterface.inputValidate("description")
        }
        else if(discussionSession.date?.trim().equals(""))
        {
            discussionInterface.inputValidate("date")
        }
        else if(discussionSession.time?.trim().equals(""))
        {
            discussionInterface.inputValidate("time")
        }
        else{
            databaseReference.child("discussionSession").push().setValue(discussionSession)
                .addOnSuccessListener {
                    discussionInterface.saveDiscussion(true)
                }
                .addOnFailureListener{
                    discussionInterface.saveDiscussion(false)
                    it.printStackTrace()
                }
        }
    }

    fun showTime(h: Int, min: Int):String {
        var format:String
        var hour = h
        if (hour == 0) {
            hour += 12
            format = "AM"
        } else if (hour == 12) {
            format = "PM"
        } else if (hour > 12) {
            hour -= 12
            format = "PM"
        } else {
            format = "AM"
        }
        return String.format("%02d:%02d $format",hour,min)
    }
}
