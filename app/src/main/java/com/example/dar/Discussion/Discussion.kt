package com.example.dar.Discussion

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.MotionEvent
import android.widget.Button
import android.widget.Toast
import com.example.dar.DiscussionInterface
import com.example.dar.AllDiscussion.AllDiscussion
import com.example.dar.R
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputEditText
import kotlinx.android.synthetic.main.activity_discussion.*

import java.util.*

class Discussion : AppCompatActivity(),DiscussionInterface {
//    lateinit var title: TextInputEditText
//    lateinit var description:TextInputEditText
//    lateinit var sessionDate:TextInputEditText
//    lateinit var sessionTime:TextInputEditText
//    lateinit var post: Button

    lateinit var discussionPresenter: DiscussionPresenter
    lateinit var calendar: Calendar
    lateinit var sharedPreferences:SharedPreferences

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_discussion)

        var toolbar = supportActionBar
        toolbar?.setTitle("Add Discussion")
        toolbar?.setDisplayHomeAsUpEnabled(true)

//        title = findViewById(R.id.title)
//        description = findViewById(R.id.description)
//        sessionDate = findViewById(R.id.date)
//        sessionTime = findViewById(R.id.time)
//        post = findViewById(R.id.post)

        discussionPresenter = DiscussionPresenter(this)
        calendar = Calendar.getInstance()

        sharedPreferences = getSharedPreferences("user_details", Context.MODE_PRIVATE)

        sessionDate.setOnTouchListener { view, motionEvent ->
            if(motionEvent.action == MotionEvent.ACTION_UP){
                datepickerdialog()
                true
            }
            false
        }
        sessionTime.setOnTouchListener { view, motionEvent ->
            if(motionEvent.action == MotionEvent.ACTION_UP){
                timePicker()
                true
            }
            false
        }

        post.setOnClickListener {
            val discussionSession = DiscussionSession(
                title = sessionTitle.text.toString(),
                description = description.text.toString(),
                date = sessionDate.text.toString(),
                time = sessionTime.text.toString(),
                uid = sharedPreferences.getString("Uid",""),
                email = sharedPreferences.getString("email",""),
                name = sharedPreferences.getString("displayName","")
            )
            MaterialAlertDialogBuilder(this)
                .setMessage("Do you want to save?")
                .setPositiveButton("yes",{dialogInterface, i ->
                    discussionPresenter.postDiscussion(discussionSession)
                })
                .setNegativeButton("no",{dialogInterface, i ->
                    dialogInterface.cancel()
                })
                .show()
        }
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        finish()
        return true
    }
    override fun saveDiscussion(rs: Boolean) {
        if(rs){
            Toast.makeText(this,"Successfully Posted!",Toast.LENGTH_LONG).show()
            val intent = Intent(this, AllDiscussion::class.java)
            startActivity(intent)
            finish()
        }
        else{
            Toast.makeText(this,"The Post failed",Toast.LENGTH_LONG).show()
        }
    }

    fun datepickerdialog(){
        val year: Int = calendar.get(Calendar.YEAR)
        val month: Int = calendar.get(Calendar.MONTH)
        val day: Int = calendar.get(Calendar.DAY_OF_MONTH)

        val picker = DatePickerDialog(this, DatePickerDialog.OnDateSetListener{ datePicker, year, month, day ->
            sessionDate.setText("$day/${month+1}/$year")
        }, year, month, day)
        picker.show()
    }

    fun timePicker(){
        val hour = calendar.get(Calendar.HOUR_OF_DAY)
        val minutes = calendar.get(Calendar.MINUTE)
        lateinit var time:String
        val picker = TimePickerDialog(this, TimePickerDialog.OnTimeSetListener {
                timePicker, selectedHour, selectedMinute ->
            sessionTime.setText(discussionPresenter.showTime(selectedHour,selectedMinute))
        },hour,minutes,false)
        picker.show()
    }

    override fun inputValidate(input: String) {
        when(input){
            "title" -> sessionTitle.error = "Please fill the fiels!"
            "description" -> description.error = "Please fill the fiels!"
            "date" -> sessionDate.error = "Please fill the fiels!"
            "time" -> sessionTime.error = "Please fill the fiels!"
        }
    }
}

