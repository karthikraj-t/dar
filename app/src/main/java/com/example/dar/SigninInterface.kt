package com.example.dar

import com.google.android.gms.common.api.GoogleApiClient

interface SigninInterface {
    fun signIn(googleApiClient: GoogleApiClient)

    fun signInfirebase(rs:Boolean)
}