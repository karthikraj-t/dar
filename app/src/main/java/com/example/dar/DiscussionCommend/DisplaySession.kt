package com.example.dar.DiscussionCommend


import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.ActionBar
import android.view.MenuItem
import android.view.View
import android.widget.*
import com.example.dar.AllDiscussion.AllDiscussion
import com.example.dar.Discussion.QA
import com.example.dar.GetQA
import com.example.dar.R
import com.example.dar.Signin.Home
import java.sql.Timestamp
import java.text.SimpleDateFormat

class DisplaySession : AppCompatActivity(),GetQA {


    lateinit var sharedPreferences: SharedPreferences
    lateinit var displaySessionPresenter: DisplaySessionPresenter
    lateinit var bundle: Bundle
    lateinit var qa:EditText
    lateinit var progressBar: ProgressBar
    var commendVisible  = true


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_display_session)

        var toolbar: ActionBar? =supportActionBar
        toolbar?.setTitle("Discussion")
        toolbar?.setDisplayHomeAsUpEnabled(true)

        sharedPreferences = getSharedPreferences("user_details", Context.MODE_PRIVATE)
        progressBar = findViewById(R.id.progress_Bar)
        progressBar.visibility = View.GONE

        val title:TextView = findViewById(R.id.title)
        val descrition:TextView = findViewById(R.id.description)
        val date:TextView = findViewById(R.id.date)
        val addCommends = findViewById<TextView>(R.id.addCommends)
        val commendSection = findViewById<RelativeLayout>(R.id.commandSection)

        qa = findViewById(R.id.qa)
        val post: Button = findViewById(R.id.post)

        val intent = intent.extras
        val sessionid = intent.get("sessionId").toString()

        bundle = Bundle()
        bundle.putString("sessionId",sessionid.toString())
        title.setText(intent.get("title").toString())
        descrition.setText(intent.get("desc").toString())
        date.setText(intent.get("name").toString()+" - "+intent.get("date").toString())

        displaySessionPresenter = DisplaySessionPresenter( this)

        loadfrag()

        addCommends.setOnClickListener{

            if(commendVisible)
            {
                commendSection.visibility = View.VISIBLE
                commendVisible = false
            }else{
                commendSection.visibility = View.GONE
                commendVisible = true
            }

        }
        post.setOnClickListener {
            val timestamp = Timestamp(System.currentTimeMillis())
            val sdf =SimpleDateFormat("dd MMM yy HH:mm")
            val qas = qa.text.toString()
            val a = QA(
                qa=qas,
                sessionId = sessionid,
                email = sharedPreferences.getString("email",""),
                name = sharedPreferences.getString("displayName",""),
                time = sdf.format(timestamp)
            )
            progressBar.visibility = View.VISIBLE
            displaySessionPresenter.postQA(a)
            commendSection.visibility = View.GONE
            commendVisible = true
        }
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        finish()
        return true
    }
    fun loadfrag(){

        val fragment = SingleDiscussionQa()
        fragment.arguments = bundle
        supportFragmentManager.beginTransaction()
            .replace(R.id.qaFragment,fragment)
            .commit()
    }
    override fun postCommand(rs: Boolean) {
        if(rs){
            qa.setText("")
            Toast.makeText(this,"Your command posted!",Toast.LENGTH_LONG).show()
            loadfrag()
            progressBar.visibility = View.GONE
        }else{
            Toast.makeText(this,"failed",Toast.LENGTH_LONG).show()
        }
    }
    override fun getQa(allqa: MutableList<QA>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
