package com.example.dar.DiscussionCommend

import com.example.dar.Discussion.QA
import com.example.dar.FirebaseDataBaseReference
import com.example.dar.GetQA
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener

class DisplaySessionPresenter(var getQA: GetQA) {

    private val databaseReference = FirebaseDataBaseReference.firebasereference()
    var allQa = mutableListOf<QA>()

    fun postQA(qa: QA){
        databaseReference.child("QA").push().setValue(qa).addOnSuccessListener {
            getQA.postCommand(true)
        }.addOnCanceledListener {
            getQA.postCommand(false)
        }
    }
    fun getqa(sessionId:String){
        databaseReference.child("QA").addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                p0.toException()
            }

            override fun onDataChange(p0: DataSnapshot) {
                val child = p0.children
                child.forEach {
                    val qa = it.getValue(QA::class.java)
                    if(qa?.sessionId.equals(sessionId)){
                        qa?.let {
                                it1 -> allQa.add(it1)
                        }
                    }
                }
                getQA.getQa(allQa)
            }
        })
    }
}