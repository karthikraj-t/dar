package com.example.dar.DiscussionCommend



import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import com.example.dar.Discussion.QA
import com.example.dar.GetQA
import com.example.dar.R
import java.text.SimpleDateFormat

class SingleDiscussionQa : Fragment(),GetQA{

    lateinit var progressBar: ProgressBar
    lateinit var recyclerView: RecyclerView
    lateinit var sessionid:String
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.activity_single_discussion_qa, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView = view.findViewById(R.id.qalist)
        progressBar = view.findViewById(R.id.progress_Bar)
        progressBar.visibility = View.VISIBLE

        sessionid = this.arguments?.get("sessionId").toString()
        DisplaySessionPresenter(this).getqa(sessionid)
    }

    override fun getQa(allqa: MutableList<QA>) {
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.adapter = ListAdapter(allqa)
        progressBar.visibility = View.GONE
    }
    override fun postCommand(rs: Boolean) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}
class ListAdapter(var allqa:MutableList<QA>): RecyclerView.Adapter<ListAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.single_discussion_item_qa,parent,false)
        return ViewHolder(v)
    }

    override fun getItemCount() =allqa.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val qa = allqa[position]
        holder.bindView(qa)
    }

    class ViewHolder(view:View): RecyclerView.ViewHolder(view) {
        var sdf = SimpleDateFormat("dd-MM-yyyy HH:mm")
        fun bindView(qa:QA){
           // val name = itemView.findViewById<TextView>(R.id.name)
            val command = itemView.findViewById<TextView>(R.id.command)
            val date = itemView.findViewById<TextView>(R.id.date)

           // name.setText(qa.name)
            command.setText(qa.qa)
            val d = qa.time?.split(" ")
            val df = d?.get(0)+" "+d?.get(1)+" "+d?.get(2)+" at "+d?.get(3)
            date.setText(qa.name+" - "+df)
        }
    }
}