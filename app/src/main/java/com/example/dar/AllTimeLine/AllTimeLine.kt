package com.example.dar.AllTimeLine

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.example.dar.R
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import kotlinx.android.synthetic.main.activity_all_time_line.*


class AllTimeLine : Fragment(),AdapterView.OnItemSelectedListener{

    lateinit var progressBar:ProgressBar
    lateinit var pieChart: PieChart
    lateinit var month:Spinner
    lateinit var year:Spinner
    lateinit var viewModel:AllTimeLineViewModel
    lateinit var sharedPreferences: SharedPreferences
    lateinit var legend: RecyclerView
    lateinit var viewAdapter: RecyclerView.Adapter<*>
    lateinit var viewManager: RecyclerView.LayoutManager


    var months = arrayOf("01","02","03","04","05","06","07","08","09","10","11","12")
    var years = arrayOf("2019","2020","2021")
    var mo:String = ""
    var ye:String = ""
    var flag = false
    var listener =0
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View?
            = inflater.inflate(R.layout.activity_all_time_line, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        progressBar = view.findViewById(R.id.progress_Bar)
        progressBar.visibility = View.VISIBLE

        month = view.findViewById(R.id.month)
        year = view.findViewById(R.id.year)
        pieChart = view.findViewById(R.id.piechart)


        legend = view.findViewById(R.id.legend)
        viewManager = LinearLayoutManager(view.context)
        legend.layoutManager = viewManager
        sharedPreferences = activity!!.getSharedPreferences("user_details", Context.MODE_PRIVATE)

        val am = ArrayAdapter(view.context, android.R.layout.simple_spinner_item, months)
        am.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        month.adapter = am


        val ay = ArrayAdapter(view.context, android.R.layout.simple_spinner_item, years)
        ay.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        year.adapter = ay


        viewModel = ViewModelProviders.of(this)[AllTimeLineViewModel::class.java]

        val date = viewModel.getDateandTime().split("-")
        month.setSelection(am.getPosition(date[0]))
        year.setSelection(ay.getPosition(date[1]))
        mo = date[0]
        ye = date[1]

        month.onItemSelectedListener = this
        year.onItemSelectedListener = this

        viewModel.searchId = sharedPreferences.getString("Uid", "")
        viewModel.searchdate = mo + "-" + ye
        viewModel.getchartValue().observe(this, Observer<MutableMap<String, Float>> { users ->
            if (users != null) {
                loadChart(users)
            }
           // Log.d("viewModel", "viewModel")
        })
    }
    fun loadChart(all: MutableMap<String, Float>) {

        var allscore = mutableListOf<Entry>()
        var score = mutableListOf<String>()
        var color  = mutableListOf(
            Color.rgb(236, 112, 99 ), // red
            Color.rgb(82, 190, 128 ),//green
            Color.rgb(52, 152, 219), //blue
            Color.rgb(220, 118, 51)) //orange
        var legendTable = mutableListOf<Triple<Int,String,String>>()
        var order = 0


        all.forEach {
            if (it.value.toInt() != 0) {
                allscore.add(Entry(it.value, order))
                score.add(it.key)
                legendTable.add(Triple(color.get(order),it.key,it.value.toString()))
                order++
            }
        }

        viewAdapter = legendView(legendTable)
        legend.adapter = viewAdapter

        var pieDataSet = PieDataSet(allscore, "")
        pieDataSet.setColors(color)
        var data = PieData(score, pieDataSet)
        pieChart.data = data
        pieChart.setDrawSliceText(false)
        pieChart.animateXY(1000, 3000)
        pieChart.setDescription("")
        pieChart.isRotationEnabled = false
        pieChart.legend.isEnabled = false

        progressBar.visibility = View.GONE
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {

        if (p0 != null) {
            if (p0.id == R.id.month) {
                mo = p0.getItemAtPosition(p2).toString()
                listener++
            } else if (p0.id == R.id.year) {
                ye = p0.getItemAtPosition(p2).toString()
                listener++
            }
        }
        if (flag) {
            viewModel.searchdate = mo + "-" + ye
            viewModel.loadChartData()
            Log.d("itemflag", "1")
        }
        if (listener == 2) {
            flag = true
        }
    }
}

class legendView(var legends:MutableList<Triple<Int,String,String>>): RecyclerView.Adapter<legendView.ViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.legend_table,parent,false)
        return ViewHolder(v)
    }

    override fun getItemCount() = legends.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView(legends[position])
    }

    class ViewHolder(view:View): RecyclerView.ViewHolder(view){
        fun bindView(legend:Triple<Int, String, String>){
            var color = itemView.findViewById<View>(R.id.color)
            var score= itemView.findViewById<TextView>(R.id.score)
            var scoreValue= itemView.findViewById<TextView>(R.id.scoreValue)

            color.setBackgroundColor(legend.first)
            score.setText(legend.second)
            scoreValue.setText(legend.third)
        }
    }

}