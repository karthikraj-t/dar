package com.example.dar.AllTimeLine

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import android.view.MenuItem
import com.example.dar.R
import com.example.dar.Timeline.TimeLine
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_time_line_bottom_nav_bar.*


class TimeLineBottomNavBar : AppCompatActivity() {
    var toolbar:ActionBar? = null
    val timeline = TimeLine()
    val allTimeLine = AllTimeLine()

    lateinit var viewModel:bottomnavViewModel



    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.today -> {

                viewModel.frag = timeline
                loadfragement(viewModel.frag as Fragment)
                toolbar?.setTitle("Today LeaderBoard")
                return@OnNavigationItemSelectedListener true
            }
            R.id.all-> {
                toolbar?.setTitle("Month Chart")
                viewModel.frag = allTimeLine
                loadfragement(viewModel.frag as Fragment)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_time_line_bottom_nav_bar)

        toolbar = supportActionBar
        toolbar?.setTitle("Today LeaderBoard")
        toolbar?.setDisplayHomeAsUpEnabled(true)

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        viewModel = ViewModelProviders.of(this)[bottomnavViewModel::class.java]
        viewModel.frag?.let {
            loadfragement(it)
        }
    }

    fun loadfragement(fragment: Fragment){
        supportFragmentManager.beginTransaction()
            .replace(R.id.frame,fragment)
            .commit()
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        finish()
        return true
    }
}

class bottomnavViewModel:ViewModel(){       // ViewModel class

    var frag: Fragment? = TimeLine()
}