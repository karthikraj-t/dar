package com.example.dar.AllTimeLine

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.dar.FirebaseDataBaseReference
import com.example.dar.WorkFrom.TimelineModel
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import java.text.SimpleDateFormat
import java.util.*

class AllTimeLineViewModel : ViewModel(){

    val firebaseDatabaseReference = FirebaseDataBaseReference.firebasereference()

    val sdf = SimpleDateFormat("HH")
    var everyMonth = mutableListOf<Long>()

    var searchId = ""
    var searchdate  =""
    var nod = 0

    private val timelineChartData:MutableLiveData<MutableMap<String,Float>> by lazy {
        MutableLiveData<MutableMap<String,Float>>().also {
            loadChartData()
        }
    }

    fun getchartValue():LiveData<MutableMap<String,Float>> = timelineChartData

    fun loadChartData(){
        everyMonth.clear()
        firebaseDatabaseReference.child("Emp").addListenerForSingleValueEvent(object :ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {
                p0.toException()
            }

            override fun onDataChange(p0: DataSnapshot) {
                val emp =p0.children
                var name =""
                for(em in emp){
                    if(searchId.equals(em.key)){
                        name = em.child("name").value.toString()
                        val timeChile:MutableIterable<DataSnapshot>  = em.child("time").children
                        for(i in timeChile){
                            if(i.key!!.contains(searchdate)){
                                val intime = i.getValue(TimelineModel::class.java)
                                if (intime != null) {
                                    intime.inTime?.let {
                                        everyMonth.add(it)
                                    }
                                }
                            }
                        }
                    }
                }
                sort(everyMonth,name)
                }
        })
    }

    fun sort(all:MutableList<Long>?,name:String){

        var score = mutableMapOf("Ten" to 0f,"Nine" to 0f,"Five" to 0f, "One" to 0f)
        all?.sortBy { it }
        all?.forEach{

                val date = sdf.format(it)
                val hours=date.toInt()
                when(hours){
                    8,9 ->{
                        var v = score.get("Ten")
                        v = v?.plus(1)
                        score.put("Ten", v!!)
                    }
                    10 ->{
                        var v = score.get("Nine")
                        v = v?.plus(1)
                        score.put("Nine", v!!)
                    }
                    11 ->{
                        var v = score.get("Five")
                        v = v?.plus(1)
                        score.put("Five", v!!)
                    }
                    else ->{
                        var v = score.get("One")
                        v = v?.plus(1)
                        score.put("One", v!!)
                    }
                }
            }
            timelineChartData.value = score
        }


    fun getDateandTime():String{
        val simpleDateFormat = SimpleDateFormat("MM-yyyy")
        val date = Date(System.currentTimeMillis())
        val today = simpleDateFormat.format(date)
        return today
    }
}